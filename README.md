# Build and Deploy

```
# Customize cardano-cli.yml with your openfaas endpoint
# Build function (docker image)
faas build -f cardano-cli.yml

# Push image
docker push repsistance/faas-cardano-cli:latest

# Deploy

export NETWORK=testnet
export CARDANO_NODE_SOCAT_HOST_URI=cardano-node-headless.dandelion-tn-v8.svc.cluster.local:30000
faas deploy -f cardano-cli.yml \
  --env=NETWORK=${NETWORK} \
  --env=CARDANO_NODE_SOCAT_HOST_URI=${CARDANO_NODE_SOCAT_HOST_URI} \
  --name ${NETWORK}-cardano-cli
export NETWORK=mainnet
export CARDANO_NODE_SOCAT_HOST_URI=cardano-node-headless.dandelion-mn-production.svc.cluster.local:30000
faas deploy -f cardano-cli.yml \
  --env=NETWORK=${NETWORK} \
  --env=CARDANO_NODE_SOCAT_HOST_URI=${CARDANO_NODE_SOCAT_HOST_URI} \
  --name ${NETWORK}-cardano-cli
```
